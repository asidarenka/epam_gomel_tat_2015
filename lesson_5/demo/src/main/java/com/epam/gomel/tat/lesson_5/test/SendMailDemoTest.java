package com.epam.gomel.tat.lesson_5.test;

import com.epam.gomel.tat.lesson_5.pages.MailInboxListPage;
import com.epam.gomel.tat.lesson_5.pages.MailLoginPage;
import com.epam.gomel.tat.lesson_5.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_5.ui.Browser;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SendMailDemoTest {
    // Test data
    private String userLogin = "tat-test-user"; // ACCOUNT
    private String userPassword = "tat-123qwe"; // ACCOUNT
    private String mailTo = "tat-test-user@yandex.ru"; // ENUM
    private String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    private String mailContent = "mail content" + Math.random() * 100000000; // RANDOM
    private static final String FILE_NAME = "D:\\ping_at_to_selenium_hub.PNG";

    @Test(description = "Send mail")
    public void sendMail() {
        MailboxBasePage mailbox = new MailLoginPage().open()
                .login(userLogin, userPassword)
                .openInboxPage()
                .openComposeMailPage()
                .sendMail(mailTo, mailSubject, mailContent, FILE_NAME);
        Assert.assertTrue(mailbox.openSentPage().isMessagePresent(mailSubject), "Mail is not in sent list");
    }

//    @AfterClass(description = "Close browser")
//    public void clearBrowser() {
//        Browser.get().kill();
//    }
}
