package ui;

import cli.TestRunnerOption;
import com.google.common.base.Predicate;
import config.YandexConfig;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import reporting.Logger;
import sun.management.counter.Units;
import sun.plugin2.util.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Art on 13.07.2015.
 */
public class Browser {
    public static final int WAIT_ELEMENT_TIME_OUT = 20;
    private static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt";
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 5;
    private static final int AJAX_TIMEOUT = 20;
    public static final String URL_PATTERN = "http://%s:%d/wd/hub";
    private WebDriver driver;
    private static Map<Thread, Browser> instances = new HashMap<>();
    private static Browser instance = null;

    private Browser(final WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            return instance;
        }
        instance = init();
        instances.put(currentThread, instance);
        return instance;
    }

    private static Browser init() {
        BrowserType browserType = TestRunnerOption.getBrowserType();
        WebDriver driver = null;
        switch(browserType){
            case REMOTE:
                Logger.info("Open REMOTE browser");
                try{
                    driver = new RemoteWebDriver(new URL(String.format(URL_PATTERN, TestRunnerOption.getInstance().getHost(),
                            TestRunnerOption.getInstance().getPort())), DesiredCapabilities.firefox());
                }catch(MalformedURLException e){
                    throw new RuntimeException("Invalid URL format");
                }
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case FIREFOX:
                Logger.info("Open FIREFOX browser");
                driver = new FirefoxDriver(getFireFoxProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case CHROME:
                Logger.info("Open CHROME browser");
                System.setProperty("webdriver.chrome.driver", TestRunnerOption.getChromeDriver());
                driver = new ChromeDriver(getChromeProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS*2, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS*2, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
        }
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", TestRunnerOption.getDownloadDir());
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    private static DesiredCapabilities getChromeProfile(){
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", TestRunnerOption.DOWNLOAD_DIR);
        chromePrefs.put("download.prompt_for_download", false);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        return capabilities;
    }

    public void open(String url) {
        Logger.info("Open URL");
        driver.get(url);
    }

    public static void kill() {
        Logger.info("Brower close");
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            try {
                instance.driver.quit();
            } catch (Exception e) {
                Logger.error("Can not kill browser", e);
            } finally {
                instances.remove(currentThread);
            }
        }
    }

    public String getText(By locator) {
        Logger.info("Get text from : " + locator);
        return driver.findElement(locator).getText();
    }

    public void click(By locator) {
        Logger.info("Click : " + locator);
        driver.findElement(locator).click();
    }

    public void type(By locator, String text) {
        Logger.info("Find element : " + locator + "SendKeys :" + text);
        driver.findElement(locator).sendKeys(text);
    }

    public void attachFile(By locator, String text) {
        Logger.info("Find element for attach : " + locator + "SendKeys :" + text);
        driver.findElement(locator).sendKeys(text);
    }

    public void waitForVisible(By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIME_OUT).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    public void waitForAjaxProccessed() {
        Logger.info("Wait for ajax proccessed");
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void refresh() {
        Logger.info("Refresh page");
        driver.navigate().refresh();
    }
}