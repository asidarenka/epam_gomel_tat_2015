package config;

/**
 * Created by Art on 26.07.2015.
 */
public enum ParallelMode {
    FALSE("false"),
    TESTS("tests"),
    CLASSES("classes"),
    METHODS("methods"),
    SUITES("suites");

    private String alias;

    private ParallelMode(String alias){
        this.alias = alias;
    }

    public static ParallelMode getTypeByAlias(String alias){
        for(ParallelMode type : ParallelMode.values()){
            if (type.getAlias().equals(alias.toLowerCase())){
                return type;
            }
        }
        throw new RuntimeException("No such runtime exeption");
    }

    public String getAlias(){
        return this.alias;
    }
}
