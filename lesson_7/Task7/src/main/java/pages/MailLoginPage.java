package pages;

import org.openqa.selenium.By;
import ui.Browser;

/**
 * Created by Art on 13.07.2015.
 */
public class MailLoginPage extends AbstractBasePage { 
    public static final String START_PAGE = "http://www.mail.yandex.ru";
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//*[@class=' nb-button _nb-action-button nb-group-start']");

    public MailLoginPage open(){
        browser.open(START_PAGE);
        return this;
    }

    public MailboxBasePage login(String login, String password){
        browser.type(LOGIN_INPUT_LOCATOR, login);
        browser.type(PASSWORD_INPUT_LOCATOR, password);
        browser.click(LOGIN_BUTTON_LOCATOR);
        return new MailboxBasePage();
    }
}