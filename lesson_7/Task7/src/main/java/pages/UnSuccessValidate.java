package pages;

import org.openqa.selenium.By;

/**
 * Created by Art on 17.07.2015.
 */
public class UnSuccessValidate extends AbstractBasePage { 
    public static final By MESSAGE_LOCATOR = By.xpath("//div[@class='error-msg']");
    private static final String ERROR_TEXT = "������������ ����� ��� ������.";

    public boolean isFindMessage(){
        return browser.getText(MESSAGE_LOCATOR).equals(ERROR_TEXT);
    }
}