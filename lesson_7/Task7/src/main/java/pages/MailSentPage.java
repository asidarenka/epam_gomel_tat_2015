package pages;

import bo.mail.MailLetter;
import org.openqa.selenium.By;
import reporting.Logger;

/**
 * Created by Art on 13.07.2015.
 */
public class MailSentPage extends AbstractBasePage { 
    private static final String MESSAGE_LINK_LOCATOR_PATTERN = "//label[text()='Sent']/ancestor::div[@class='block-messages']//a[.//*[text()='%s']]";

    public void openMessage(String subject){
        String mailLocator = String.format(MESSAGE_LINK_LOCATOR_PATTERN, subject);
        browser.click(By.xpath(mailLocator));
        Logger.log.info("Open letter : " + subject);
    }
}