package pages;

import ui.Browser;

/**
 * Created by Art on 13.07.2015.
 */
public abstract class AbstractBasePage { 
    protected Browser browser;

    public AbstractBasePage(){
        this.browser = Browser.get();
    }
}