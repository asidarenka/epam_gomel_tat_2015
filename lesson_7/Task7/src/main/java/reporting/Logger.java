package reporting;

/**
 * Created by Art on 17.07.2015.
 */
public class Logger { 
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Logger.class);

    public static void trace(String s, Throwable t) {
        log.trace(s, t);
    }

    public static void trace(String s) {
        log.trace(s);
    }

    public static void error(String s, Throwable t) {
        log.error(s, t);
    }

    public static void error(String s) {
        log.error(s);
    }

    public static void info(String s, Throwable t) {
        log.info(s, t);
    }

    public static void info(String s) {
        log.info(s);
    }

    public static void fatal(String s, Throwable t) {
        log.fatal(s, t);
    }

    public static void fatal(String s) {
        log.fatal(s);
    }
}