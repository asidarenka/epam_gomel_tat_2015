package runner;

import cli.TestRunnerOption;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestNG;
import reporting.CustomTestNgListener;
import reporting.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Art on 17.07.2015.
 */
public class Runner { 
    public static void main(String []args){
        Runner runner = new Runner();
        runner.parseCli(args);
        runner.runTest();
    }

    private void parseCli(String []args){
        CmdLineParser parser = new CmdLineParser(TestRunnerOption.getInstance());
        try{
            parser.parseArgument(args);
        }catch(CmdLineException e){
            Logger.error("Can't parse arguments" + e);
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
            System.err.println();
        }
    }

    private void runTest(){
        TestNG test = new TestNG();
        test.addListener(new CustomTestNgListener());
        List<String> suites = new ArrayList<String>();
        for(String suite : TestRunnerOption.getInstance().getSuitesFiles()){
            suites.add(suite);
        }
        test.setTestSuites(suites);
        test.run();
    }
}