package test;

import bo.common.AccountBuilder;
import bo.mail.MailLetter;
import bo.mail.MailLetterBuilder;
import org.testng.annotations.Test;
import service.LoginService;
import service.MailService;

/**
 * Created by Art on 16.07.2015.
 */
public class SendMailWithAttachTest { 
    MailLetter letter = MailLetterBuilder.getLetterWithAttach();
    private LoginService loginService = new LoginService();
    private MailService mailService = new MailService();

    @Test(description = "open start page and login to account")
    public void loginToAccount(){
        loginService.loginToAccount(AccountBuilder.getDefaultAccount());
    }

    @Test(description = "send message with attach", dependsOnMethods = "loginToAccount")
    public void sendMail(){
        mailService.sendMailWithAttach(letter);
    }

    @Test(description = "check letter", dependsOnMethods = "sendMail")
    public void checkLetter(){
        mailService.checkMailWithAttachInSentList(letter);
    }
}