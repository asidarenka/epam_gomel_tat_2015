package test;

import bo.common.AccountBuilder;
import bo.mail.MailLetter;
import bo.mail.MailLetterBuilder;
import org.testng.annotations.Test;
import service.LoginService;
import service.MailService;

/**
 * Created by Art on 16.07.2015.
 */
public class MarkAsNotSpamTest { 
    MailLetter letter = MailLetterBuilder.getLetter();
    private LoginService loginService = new LoginService();
    private MailService mailService = new MailService();

    @Test(description = "Open start page and login to account.")
    public void loginToAccount(){
        loginService.loginToAccount(AccountBuilder.getDefaultAccount());
    }

    @Test(description = "Send message.", dependsOnMethods = "loginToAccount")
    public void sendMail(){
        mailService.sendMail(letter);
    }

    @Test(description = "Check content of letter.", dependsOnMethods = "sendMail")
    public void checkLetter(){
        mailService.checkMailInSentList(letter);
    }

    @Test(description = "Open Inbox list and mark as spam the letter.", dependsOnMethods = "checkLetter")
    public void markLetterAsSpam(){
        mailService.markAsSpam(letter);
    }

    @Test(description = "Check letter in spam list.", dependsOnMethods = "markLetterAsSpam")
    public void checkSpamLetter(){
        mailService.checkMailInSpamList(letter);
    }

    @Test(description = "Mark letter as not spam.", dependsOnMethods = "markLetterAsSpam")
    public void markLetterAsNotSpam(){
        mailService.markAsNotSpam(letter);
    }

    @Test(description = "Check letter in Inbox list.", dependsOnMethods = "markLetterAsNotSpam")
    public void checkLetterInInboxList(){
        mailService.checkLetterInbox(letter);
    }
}