package test;

import bo.common.AccountBuilder;
import org.testng.annotations.Test;
import service.LoginService;
import service.MailService;

/**
 * Created by Art on 17.07.2015.
 */
public class LoginToMailTest { 
    private LoginService loginService = new LoginService();

    @Test(description = "open start page and login to account")
    public void loginToAccount(){
        loginService.loginToAccount(AccountBuilder.getDefaultAccount());
    }
}
