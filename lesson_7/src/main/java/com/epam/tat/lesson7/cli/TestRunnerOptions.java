package com.epam.tat.lesson7.cli;

import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

import java.util.List;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class TestRunnerOptions {

    @Option(name = "-bt", usage = "browser type", required = true)
    public String browserType;

    @Option(name = "-tc", usage = "thread count")
    public int threadCount;

    @Option(name = "-mode", usage = "parallel mode: false, tests, classes")
    public String parallelMode;

    @Option(name = "-suites", handler = StringArrayOptionHandler.class)
    public List<String> suites;

}
