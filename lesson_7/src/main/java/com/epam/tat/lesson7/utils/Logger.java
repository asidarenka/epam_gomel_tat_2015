package com.epam.tat.lesson7.utils;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class Logger {

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("com.epam");

    public static void info(String message) {
        logger.info(message);
    }
}
