package com.epam.tat.lesson7;

import com.epam.tat.lesson7.browser.Browser;

import com.epam.tat.lesson7.page.GooglePage;
import com.epam.tat.lesson7.page.PriorHomePage;
import com.epam.tat.lesson7.utils.Logger;
import com.epam.tat.lesson7.utils.Timeout;
import org.openqa.selenium.By;
import org.testng.annotations.*;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class ExampleTest {

    @BeforeClass
    public void prepareBrowser() {
        Browser.get();
    }

    @Test(description = "Try to open google page")
    @Parameters(value = "number")
    public void openPage(@Optional(value = "0") String number) {
        Logger.info("This test have try: " + number);
        GooglePage.open();
        Timeout.sleep(2);
//        Logger.info("1");
    }

    @Test(description = "Perform login to Prior")
    public void performLoginToPrior() {
        PriorHomePage.open().login("user", "1q2w3e").checkAntiRobotWorks();
//        Logger.info("2");
    }

//    @Test(description = "Oper")
//    public void performOperation() {
//        Logger.info("3");
//    }

    @AfterClass
    public void stopBrowser() {
        Browser.kill();
    }
}
