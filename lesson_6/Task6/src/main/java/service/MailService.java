package service;

import bo.mail.MailLetter;
import pages.CheckContent;
import pages.MailboxBasePage;
import reporting.Logger;
import utils.GenerationLetter;

/**
 * Created by Art on 16.07.2015.
 */
public class MailService { 
    public void sendMail(MailLetter letter){
        Logger.log.info("Sending mail to : " + letter.getMailTo());
        MailboxBasePage mailPage = new MailboxBasePage();
        mailPage.openInboxPage().openComposeMailPage().sendMail(letter);
    }

    public void sendMailWithAttach(MailLetter letter){
        Logger.log.info("Sending mail to : " + letter.getMailTo());
        MailboxBasePage mailPage = new MailboxBasePage();
        mailPage.openInboxPage().openComposeMailPage().sendMailWithAttach(letter);
    }

    public void checkMailInSentList(MailLetter letter){
        Logger.log.info("Check mail in sent list : " + letter.getSubject());
        MailboxBasePage mailPage = new MailboxBasePage();
        mailPage.openSentPage().openMessage(letter.getSubject());
        new CheckContent().checkLetter(letter);
    }

    public void checkMailWithAttachInSentList(MailLetter letter){
        Logger.log.info("Check mail with attach in sent list : " + letter.getSubject());
        MailboxBasePage mailPage = new MailboxBasePage();
        mailPage.openSentPage().openMessage(letter.getSubject());
        new CheckContent().checkAttachFile(letter);
    }

    public void deleteLetter(MailLetter letter){
        Logger.log.info("Delete mail with subject : " + letter.getSubject());
        MailboxBasePage mailPage = new MailboxBasePage();
        mailPage.openInboxPage().openLetter(letter.getSubject()).deleteLetterPage();
    }

    public void checkMailInDeleteList(MailLetter letter){
        Logger.log.info("Check deleted mail in delete list with subject : " + letter.getSubject());
        MailboxBasePage mailPage = new MailboxBasePage();
        mailPage.openDeletedPage().searchLetter(letter.getSubject());
    }

    public void markAsSpam(MailLetter letter){
        Logger.log.info("mark as spam mail with subject : " + letter.getSubject());
        MailboxBasePage mailPage = new MailboxBasePage();
        mailPage.openInboxPage().openLetter(letter.getSubject()).markAsSpamPage();
    }

    public void checkMailInSpamList(MailLetter letter) {
        Logger.log.info("Check spam mail in spam list with subject : " + letter.getSubject());
        MailboxBasePage mailPage = new MailboxBasePage();
        mailPage.openSpamPage();
    }

    public void markAsNotSpam(MailLetter letter) {
        Logger.log.info("mark as not spam mail with subject : " + letter.getSubject());
        MailboxBasePage mailPage = new MailboxBasePage();
        mailPage.openSpamPage().openMessage(letter.getSubject()).markAsNotSpam();
    }

    public void checkLetterInbox(MailLetter letter) {
        Logger.log.info("Check not spam mail in box list with subject : " + letter.getSubject());
        MailboxBasePage mailPage = new MailboxBasePage();
        mailPage.openInboxPage().searchLetter(letter.getSubject());
    }
}