package pages;

import bo.mail.MailLetter;
import org.openqa.selenium.By;
import reporting.Logger;
import sun.rmi.runtime.Log;
import ui.Browser;
import utils.GenerationLetter;

/**
 * Created by Art on 16.07.2015.
 */
public class CheckContent extends AbstractBasePage { 
    private static final By SUBJECT_LINK_LOCATOR = By.xpath("//span[@class = 'js-message-subject js-invalid-drag-target']");
    private static final By CONTENT_LINK_LOCATOR = By.xpath("//div[@class = 'b-message-body__content']");
    private static final By DOWNLOAD_LINK_LOCATOR = By.xpath("//a[@class='b-link b-link_w b-link_js b-file__download js-attachments-get-btn daria-action']");
    private static final String PATH_TO_FILE = "D:\\";

    public void checkLetter(MailLetter letter){
        if (!letter.getSubject().equals(browser.getText(SUBJECT_LINK_LOCATOR))){
           Logger.log.error("Does not match subject : " + letter.getSubject());
        }
        if(!letter.getContent().equals(browser.getText(CONTENT_LINK_LOCATOR))){
            Logger.log.error("Does not match content : " + letter.getContent());
        }
    }

    public void checkAttachFile(MailLetter letter){
        if (!letter.getSubject().equals(browser.getText(SUBJECT_LINK_LOCATOR))){
            Logger.log.error("Does not match subject : " + letter.getSubject());
        }
        if(!letter.getContent().equals(browser.getText(CONTENT_LINK_LOCATOR))){
            Logger.log.error("Does not match content : " + letter.getContent());
        }
        browser.click(DOWNLOAD_LINK_LOCATOR);
        browser.refresh();
        browser.waitForAjaxProccessed();
        try {
            if(!GenerationLetter.compareFiles(PATH_TO_FILE + letter.getFileName(),
                    browser.getDownloadDir() + letter.getFileName())){
                Logger.log.info("The contents of the files are the same");
        }
        }catch(Exception e){
        }
    }
}