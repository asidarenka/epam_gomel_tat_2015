package pages;

import bo.mail.MailLetter;
import org.openqa.selenium.By;
import reporting.Logger;
import utils.GenerationLetter;

import java.io.IOException;

/**
 * Created by Art on 13.07.2015.
 */
public class ComposeMailPage extends AbstractBasePage { 
    private static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    private static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    private static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    private static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    private static final By ATTACHE_FILE_INPUT_LOCATOR = By.xpath("//input[@name='att']");
    private static final String PATH_TO_FILE = "D:\\";

    public MailboxBasePage sendMail(MailLetter letter){
        browser.type(TO_INPUT_LOCATOR, letter.getMailTo());
        browser.type(SUBJECT_INPUT_LOCATOR, letter.getSubject());
        browser.type(MAIL_TEXT_LOCATOR, letter.getContent());
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
        browser.waitForAjaxProccessed();
        Logger.log.info("Mail sended");
        return new MailboxBasePage();
    }

    public MailboxBasePage sendMailWithAttach(MailLetter letter) {
        browser.type(TO_INPUT_LOCATOR, letter.getMailTo());
        browser.type(SUBJECT_INPUT_LOCATOR, letter.getSubject());
        browser.type(MAIL_TEXT_LOCATOR, letter.getContent());
        GenerationLetter.randomFile(PATH_TO_FILE + letter.getFileName(), 20);
        browser.attachFile(ATTACHE_FILE_INPUT_LOCATOR, PATH_TO_FILE + letter.getFileName());
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
        browser.waitForAjaxProccessed();
        Logger.log.info("Mail sended with attach");
        return new MailboxBasePage();
    }
}