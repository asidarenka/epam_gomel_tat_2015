package pages;

import org.openqa.selenium.By;

/**
 * Created by Art on 13.07.2015.
 */
public class MailDeletedPage extends AbstractBasePage { 
    private static final String MESSAGE_LINK_LOCATOR_PATTERN = "//label[text()='Trash']/ancestor::div[@class='block-messages']//a[.//*[text()='%s']]";

    public void searchLetter(String subject){
        String mailLocator = String.format(MESSAGE_LINK_LOCATOR_PATTERN, subject);
        browser.waitForVisible(By.xpath(mailLocator));
    }
}