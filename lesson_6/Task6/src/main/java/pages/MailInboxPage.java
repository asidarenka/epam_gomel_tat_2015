package pages;

import org.openqa.selenium.By;
import reporting.Logger;

/**
 * Created by Art on 13.07.2015.
 */
public class MailInboxPage extends AbstractBasePage { 
    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final String MESSAGE_LINK_LOCATOR_PATTERN = "//div[@class='block-messages-title']//span[text()='test201507@yandex.ru']/ancestor::div[@class='block-messages']//a[.//*[text()='%s']]";
    private static final By DELETE_LINK_LOCATOR = By.xpath("//a[@title='Delete (Delete)']");
    private static final By SPAM_LINK_LOCATOR = By.xpath("//a[@title='Spam! (Shift + s)']");

    public ComposeMailPage openComposeMailPage(){
        Logger.log.info("Open compose page");
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }

    public MailInboxPage openLetter(String subject){
        String mailLocator = String.format(MESSAGE_LINK_LOCATOR_PATTERN, subject);
        browser.click(By.xpath(mailLocator));
        return new MailInboxPage();
    }

    public void searchLetter(String subject){
        browser.waitForAjaxProccessed();
        String mailLocator = String.format(MESSAGE_LINK_LOCATOR_PATTERN, subject);
        browser.waitForVisible(By.xpath(mailLocator));
    }

    public void deleteLetterPage(){
        browser.click(DELETE_LINK_LOCATOR);
        browser.waitForAjaxProccessed();
    }

    public void markAsSpamPage(){
        browser.click(SPAM_LINK_LOCATOR);
        browser.waitForAjaxProccessed();
    }
}