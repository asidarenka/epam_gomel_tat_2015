package test;

import bo.common.AccountBuilder;
import bo.mail.MailLetter;
import bo.mail.MailLetterBuilder;
import org.testng.annotations.Test;
import service.LoginService;
import service.MailService;

/**
 * Created by Art on 16.07.2015.
 */
public class DeleteMailTest { 
    MailLetter letter = MailLetterBuilder.getLetter();
    private LoginService loginService = new LoginService();
    private MailService mailService = new MailService();

    @Test(description = "Open start page and login to account.")
    public void loginToAccount(){
        loginService.loginToAccount(AccountBuilder.getDefaultAccount());
    }

    @Test(description = "Send message.", dependsOnMethods = "loginToAccount")
    public void sendMail(){
        mailService.sendMail(letter);
    }

    @Test(description = "Open Inbox list and delete letter.", dependsOnMethods = "sendMail")
    public void deleteLetter(){
        mailService.deleteLetter(letter);
    }

    @Test(description = "Check letter in deleted list.", dependsOnMethods = "deleteLetter")
    public void checkDeletedLetter(){
        mailService.checkMailInDeleteList(letter);
    }
}