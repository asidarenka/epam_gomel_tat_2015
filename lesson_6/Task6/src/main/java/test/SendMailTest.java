package test;

import bo.common.AccountBuilder;
import bo.mail.MailLetter;
import bo.mail.MailLetterBuilder;
import org.testng.annotations.Test;
import service.LoginService;
import service.MailService;
import ui.Browser;

/**
 * Created by Art on 13.07.2015.
 */
public class SendMailTest { 
    MailLetter letter = MailLetterBuilder.getLetter();
    private LoginService loginService = new LoginService();
    private MailService mailService = new MailService();

    @Test(description = "open start page and login to account")
    public void loginToAccount(){
        loginService.loginToAccount(AccountBuilder.getDefaultAccount());
    }

    @Test(description = "send message", dependsOnMethods = "loginToAccount")
    public void sendMail(){
        mailService.sendMail(letter);
    }

    @Test(description = "Check content of letter", dependsOnMethods = "sendMail")
    public void checkLetter(){
        mailService.checkMailInSentList(letter);
    }
}