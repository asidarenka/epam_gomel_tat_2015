package bo.common;

/**
 * Created by Art on 14.07.2015.
 */
public class Account { 
    private String login;
    private String password;
    private String mail;

    public Account(String login, String password, String mail){
        this.login = login;
        this.password = password;
        this.mail = mail;
    }

    public String getLogin(){
        return this.login;
    }

    public void setLogin(String login){
        this.login = login;
    }

    public String getPassword(){
        return this.password;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public String getMail(){
        return this.mail;
    }

    public void setMail(String mail){
        this.mail = mail;
    }
}