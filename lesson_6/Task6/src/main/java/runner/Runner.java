package runner;

import org.testng.TestNG;
import reporting.CustomTestNgListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Art on 17.07.2015.
 */
public class Runner { 
    public static void main(String []args){
        List<String> suites = new ArrayList<String>(Arrays.asList(args));
        TestNG testNG = new TestNG();
        testNG.addListener(new CustomTestNgListener());
        testNG.setTestSuites(suites);
        testNG.run();
    }
}