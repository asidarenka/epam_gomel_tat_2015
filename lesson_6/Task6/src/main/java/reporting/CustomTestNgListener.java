package reporting;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener2;
import ui.Browser;

/**
 * Created by Art on 17.07.2015.
 */
public class CustomTestNgListener implements IResultListener2 { 
    public void onStart(ITestContext context) {
        Logger.log.info("Test start:" + context.getCurrentXmlTest().getName());
    }

    public void onFinish(ITestContext context) {
        Browser.get().kill();
        Logger.info("FINISH : " + context.getName());
    }

    public void onTestStart(ITestResult result) {
        Logger.info("TEST METHOD START : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    public void onTestSuccess(ITestResult result) {
        Logger.info("TEST METHOD SUCCESS : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    public void onTestFailure(ITestResult result) {
        Logger.log.info("TEST METHOD FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("TEST METHOD FAILED : ", result.getThrowable());
    }

    public void onTestSkipped(ITestResult result) {
        Logger.log.info("TEST METHOD SKIPPED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("TEST METHOD SKIPPED : ", result.getThrowable());
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        Logger.log.info("TEST METHOD FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("TEST METHOD FAILED : " + result.getThrowable());
    }

    public void beforeConfiguration(ITestResult result) {
        Logger.log.info("CONFIG START : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    public void onConfigurationSuccess(ITestResult result) {
        Logger.log.info("CONFIG SUCCESS : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    public void onConfigurationFailure(ITestResult result) {
        Logger.log.info("CONFIG FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("CONFIG FAILED : ", result.getThrowable());
    }

    public void onConfigurationSkip(ITestResult result) {
        Logger.log.info("CONFIG SKIPPED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }
}