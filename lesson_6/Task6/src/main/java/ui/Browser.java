package ui;

import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import reporting.Logger;
import sun.management.counter.Units;
import java.util.concurrent.TimeUnit;

/**
 * Created by Art on 13.07.2015.
 */
public class Browser { 
    public static final int WAIT_ELEMENT_TIME_OUT = 20;
    private static final String DOWNLOAD_DIR = "d:\\TMP\\";
    private static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt";
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 5;
    private static final int AJAX_TIMEOUT = 20;
    private WebDriver driver;
    private static Browser instance = null;

    private Browser(final WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        if (instance != null) {
            return instance;
        }
        return instance = init();
    }

    private static Browser init() {
        WebDriver driver = new FirefoxDriver(getFireFoxProfile());
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        Logger.info("Browser init");
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", DOWNLOAD_DIR);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    public String getDownloadDir() {
        return this.DOWNLOAD_DIR;
    }

    public void open(String url) {
        driver.get(url);
        Logger.info("Open URL");
    }

    public static void kill() {
        if (instance != null) {
            try {
                instance.driver.quit();
                Logger.info("Brower close");
            } catch (Exception e) {
                Logger.error("Can not kill browser", e);
            } finally {
                instance = null;
            }
        }
    }

    public String getText(By locator) {
        Logger.info("Get text from : " + locator);
        return driver.findElement(locator).getText();
    }

    public void click(By locator) {
        driver.findElement(locator).click();
        Logger.info("Click : " + locator);
    }

    public void type(By locator, String text) {
        Logger.info("Find element : " + locator + "SendKeys :" + text);
        driver.findElement(locator).sendKeys(text);
    }

    public void attachFile(By locator, String text) {
        Logger.info("Find element for attach : " + locator + "SendKeys :" + text);
        driver.findElement(locator).sendKeys(text);
    }

    public void waitForVisible(By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIME_OUT).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    public void waitForAjaxProccessed() {
        Logger.info("Wait for ajax proccessed");
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void refresh() {
        driver.navigate().refresh();
        Logger.info("Refresh page");
    }
}