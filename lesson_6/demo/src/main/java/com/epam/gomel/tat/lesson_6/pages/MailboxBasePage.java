package com.epam.gomel.tat.lesson_6.pages;

import org.openqa.selenium.By;

public class MailboxBasePage extends AbstractPage {

    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    private static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");

    public MailInboxListPage openInboxPage() {
        browser.click(INBOX_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public MailSentListPage openSentPage() {
        browser.click(SENT_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailSentListPage();
    }

    public MailboxBasePage open() {
        browser.open(MailLoginPage.BASE_URL);
        return new MailboxBasePage();
    }

    public String getUserEmail() {
        return null;
    }
}
