package com.epam.tat.lesson8.utils;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class Logger {

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("com.epam");

    public synchronized static void info(String message) {
        logger.info(message);
    }
}
