package test;

import bo.common.AccountBuilder;
import org.testng.Assert;
import org.testng.annotations.Test;
import service.LoginService;

/**
 * Created by Art on 17.07.2015.
 */
public class UnsuccessLoginToMailTest { 
    private LoginService loginService = new LoginService();

    @Test(description = "open start page and login to account")
    public void loginToAccount(){
        loginService.unsuccessloginToAccount(AccountBuilder.getIncorrectAccount());
    }
}