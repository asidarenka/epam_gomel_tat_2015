package tests;

import bo.attach.AttachFile;
import bo.attach.AttachFileBuilder;
import bo.common.Account;
import bo.common.AccountBuilder;
import bo.mail.MailLetter;
import bo.mail.MailLetterBuilder;
import cli.TestRunnerOption;
import org.testng.annotations.Test;
import service.DiskService;
import service.LoginDiskService;
import ui.Browser;
import utils.GenerationLetter;

import java.io.File;

/**
 * Created by Art on 10.08.2015.
 */
public class LoginToDisk {
    public Account account = AccountBuilder.getDefaultAccount();
    public AttachFile attach = AttachFileBuilder.getFile();
    public DiskService diskService = new DiskService();

    @Test(description = "Login to yandex disk")
    public void login(){
        LoginDiskService log = new LoginDiskService();
        log.IsLogin(account.getLogin(),account.getPassword());
    }

    @Test(description = "Upload file", dependsOnMethods = "login")
    public void uploadFile(){
        diskService.uploadFile(attach);
    }

    @Test(description = "delete file", dependsOnMethods = "uploadFile")
    public void deleteFile(){
        diskService.deleteFile(attach);
    }

    @Test(description = "delete file", dependsOnMethods = "deleteFile")
    public void restoreFile(){
        diskService.restoreFile(attach);
    }

}
