package service;

import bo.mail.MailLetter;
import cli.TestRunnerOption;
import org.openqa.selenium.By;
import pages.AbstractBasePage;
import reporting.Logger;
import sun.rmi.runtime.Log;
import ui.Browser;
import utils.GenerationLetter;

/**
 * Created by Art on 16.07.2015.
 */
public class CheckContentService{
    private static final By SUBJECT_LINK_LOCATOR = By.xpath("//span[@class = 'js-message-subject js-invalid-drag-target']");
    private static final By CONTENT_LINK_LOCATOR = By.xpath("//div[@class = 'b-message-body__content']");
    private static final By DOWNLOAD_LINK_LOCATOR = By.xpath("//a[@class='b-link b-link_w b-link_js b-file__download js-attachments-get-btn daria-action']");
    private static final String PATH_TO_FILE = "D:\\";

    public void checkLetter(MailLetter letter){
        if (!letter.getSubject().equals(Browser.get().getText(SUBJECT_LINK_LOCATOR))){
           Logger.log.error("Does not match subject : " + letter.getSubject());
        }
        if(!letter.getContent().equals(Browser.get().getText(CONTENT_LINK_LOCATOR))){
            Logger.log.error("Does not match content : " + letter.getContent());
        }
    }

    public void checkAttachFile(MailLetter letter){
        if (!letter.getSubject().equals(Browser.get().getText(SUBJECT_LINK_LOCATOR))){
            Logger.log.error("Does not match subject : " + letter.getSubject());
        }
        if(!letter.getContent().equals(Browser.get().getText(CONTENT_LINK_LOCATOR))){
            Logger.log.error("Does not match content : " + letter.getContent());
        }
        Browser.get().click(DOWNLOAD_LINK_LOCATOR);
        Browser.get().refresh();
        Browser.get().waitForAjaxProccessed();
        try {
            if(GenerationLetter.compareFiles(PATH_TO_FILE + letter.getFileName(),
                    TestRunnerOption.getDownloadDir() + letter.getFileName())){
                Logger.log.info("The contents of the files are the same");
        }
        }catch(Exception e){
        }
    }
}