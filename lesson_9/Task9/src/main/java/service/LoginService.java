package service;

import bo.common.Account;
import exeption.CommonRuntimeExeption;
import pages.MailLoginPage;
import pages.MailboxBasePage;
import pages.YandexPassportPage;
import reporting.Logger;
import ui.Browser;

/**
 * Created by Art on 14.07.2015.
 */
public class LoginService { 
    public void loginToAccount(Account account){
        Logger.info("Login to account " + account.getMail());
        MailboxBasePage mailPage = new MailLoginPage().open().login(account.getLogin(), account.getPassword());
        String userName = mailPage.getUserLoginName();
        if (!userName.equals(account.getMail()) && userName == null){
            new CommonRuntimeExeption("Not the account");
        }
    }

    public void unsuccessloginToAccount(Account account){
        Logger.info("Login to account " + account.getMail());
        new MailLoginPage().open().login(account.getLogin(), account.getPassword());
        if (!new YandexPassportPage().isFindMessage()){
            Logger.log.error("Wrong error message");
        }
    }
}