package service;

import bo.attach.AttachFile;
import pages.YandexDiskBasePage;
import pages.YandexDiskTrashPage;
import reporting.Logger;
import ui.Browser;

import java.io.File;

/**
 * Created by Art on 22.08.2015.
 */
public class DiskService {
    public void uploadFile(AttachFile attachFile){
        YandexDiskBasePage yandexDisk = new YandexDiskBasePage();
        yandexDisk.uploadFile(attachFile.getAbsolutePath(), attachFile.getName());
        yandexDisk.downloadFile(attachFile.getName());
        Browser.get().waitForDownload(new File(attachFile.getAbsolutePath()));
    }

    public void deleteFile(AttachFile attach){
        YandexDiskBasePage yandexDiskBasePage = new YandexDiskBasePage();
        yandexDiskBasePage.goToTrashFile(attach.getName());
    }

    public void restoreFile(AttachFile attach){
        YandexDiskTrashPage trashPage = new YandexDiskBasePage().openTrashPage();
        trashPage.restoreFile(attach.getName());
        Browser.get().waitForAjaxProccessed();
        if (!trashPage.openBasePage().isFilePresent(attach.getName())){
            Logger.error("File didn't restore. Name of file's" + attach.getName());
            throw new RuntimeException("File didn't restore");
        }
    }
}
