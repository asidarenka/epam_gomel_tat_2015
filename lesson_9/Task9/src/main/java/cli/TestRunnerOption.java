package cli;

import config.ParallelMode;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;
import ui.BrowserType;
import org.apache.commons.io.FileUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Art on 25.07.2015.
 */
public class TestRunnerOption {
    public static final String DOWNLOAD_DIR = FileUtils.getTempDirectoryPath();
    private static TestRunnerOption instance;
    @Option(name = "-driver", usage = "chrome driver")
    private static String chromeDriver = "src/main/resources/chromedriver.exe";
    @Option(name = "-bt", usage = "browser type", required = true)
    private static BrowserType browserType = BrowserType.FIREFOX;
    @Option(name = "-mode", usage = "parallel: false, tests, classes")
    private static ParallelMode parallelMode = ParallelMode.FALSE;
    @Option(name = "-tc", usage = "thread count")
    private static int threadCount = 1;
    @Option(name = "-host", usage = "remote host")
    private static String host = "localhost";
    @Option(name = "port", usage = "remote port")
    private static String port = "4444";
    @Option(name = "-suites",handler = StringArrayOptionHandler.class, required = true)
    private static List<String> suitesFiles = new ArrayList<String>();

    public static TestRunnerOption getInstance(){
        if (instance == null){
            instance = new TestRunnerOption();
        }
        return instance;
    }

    public static String getDownloadDir() {
        return DOWNLOAD_DIR;
    }

    public static String getChromeDriver() {
        return chromeDriver;
    }

    public static BrowserType getBrowserType() {
        return browserType;
    }

    public static ParallelMode getParallelMode() {
        return parallelMode;
    }

    public static int getThreadCount() {
        return threadCount;
    }

    public static String getHost() {
        return host;
    }

    public static String getPort() {
        return port;
    }

    public static List<String> getSuitesFiles() {
        return suitesFiles;
    }
}
