package utils;

import java.io.File;
import java.io.IOException;

import cli.TestRunnerOption;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by Art on 16.07.2015.
 */
public class GenerationLetter { 
    public static String getRandomString(int size){
        return RandomStringUtils.randomAlphanumeric(size);
    }

    public static void randomFile(String path, int messageLengh) {
        try {
            File file = new File(path);
            FileUtils.writeStringToFile(file, RandomStringUtils.randomAlphanumeric(messageLengh));
        }catch(IOException e){
            System.err.print("File error: " + e);
        }
    }

    public static File getRandomFile(String path, int messageLengh) {
        try {
            File file = new File(path);
            FileUtils.writeStringToFile(file, RandomStringUtils.randomAlphanumeric(messageLengh));
            return file;
        }catch(IOException e){
            System.err.print("File error: " + e);
            return null;
        }
    }

    public static boolean compareFiles(String path, String path2){
        try {
            return FileUtils.contentEquals(new File(path), new File(path2));

        }catch(IOException e){
            System.err.print("Compare error: " + e);
            return false;
        }
    }
}