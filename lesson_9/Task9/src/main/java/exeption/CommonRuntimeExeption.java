package exeption;

/**
 * Created by Art on 14.07.2015.
 */
public class CommonRuntimeExeption extends RuntimeException { 
    public CommonRuntimeExeption(String s){
        super(s);
    }

    public CommonRuntimeExeption(String s, Throwable cause){
        super(s, cause);
    }
}