package bo.attach;

import cli.TestRunnerOption;
import utils.GenerationLetter;

/**
 * Created by Art on 23.08.2015.
 */
public class AttachFile {
    private String name;
    private String path;
    private String absolutePath;

    public AttachFile(String path,String name){
        this.name = name;
        this.path = path;
        this.absolutePath = path + name + "\\";
        GenerationLetter.randomFile(TestRunnerOption.DOWNLOAD_DIR + name, 20);
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }
}
