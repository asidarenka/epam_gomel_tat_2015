package bo.attach;

import cli.TestRunnerOption;

/**
 * Created by Art on 23.08.2015.
 */
public class AttachFileBuilder {
    public static AttachFile getFile(){
        AttachFile letter = new AttachFile(TestRunnerOption.DOWNLOAD_DIR,
                "File"+(int)(Math.random()*100) +".txt");
        return letter;
    }
}
