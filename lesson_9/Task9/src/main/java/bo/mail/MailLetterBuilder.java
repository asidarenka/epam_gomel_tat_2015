package bo.mail;

import utils.GenerationLetter;

/**
 * Created by Art on 16.07.2015.
 */
public class MailLetterBuilder { 
    public static MailLetter getLetter(){
        MailLetter letter = new MailLetter("test201507@yandex.ru",
                GenerationLetter.getRandomString(7),
                GenerationLetter.getRandomString(30));
        return letter;
    }

     public static MailLetter getLetterWithAttach(){
         MailLetter letter = new MailLetter("test201507@yandex.ru",
                 GenerationLetter.getRandomString(7),
                 GenerationLetter.getRandomString(30),
                 "File"+(int)(Math.random()*100) +".txt");
         return letter;
    }
}