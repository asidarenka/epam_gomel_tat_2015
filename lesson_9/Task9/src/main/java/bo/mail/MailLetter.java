package bo.mail;

/**
 * Created by Art on 14.07.2015.
 */
public class MailLetter { 
    private String subject;
    private String mailTo;
    private String content;
    private String fileName;

    public MailLetter(String mailTo, String subject, String content){
        this.mailTo = mailTo;
        this.subject = subject;
        this.content = content;
    }

    public MailLetter(String mailTo, String subject, String content,String fileName){
        this.mailTo = mailTo;
        this.subject = subject;
        this.content = content;
        this.fileName = fileName;
    }

    public String getMailTo(){
        return this.mailTo;
    }

    public void setMailTo(String mailTo){
        this.mailTo = mailTo;
    }

    public String getSubject(){
        return this.subject;
    }

    public void setSubject(String subject){
        this.subject = subject;
    }

    public String getContent(){
        return this.content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public String getFileName(){
        return this.fileName;
    }

    public void setFileName(String fileName){
        this.fileName = fileName;
    }
}