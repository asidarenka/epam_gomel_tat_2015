package bo.common;

/**
 * Created by Art on 14.07.2015.
 */
public class AccountBuilder { 
    public static Account getDefaultAccount(){
        return Accounts.DEFAULT_MAIL.getAccount();
    }

    public static Account getIncorrectAccount(){
        return Accounts.DEFAULT_INCORRECT_MAIL.getAccount();
    }
}