package bo.common;

/**
 * Created by Art on 14.07.2015.
 */
public enum Accounts { 
    DEFAULT_MAIL("test201507","test2015","test201507@yandex.ru"),
    DEFAULT_INCORRECT_MAIL("test201507","test","test201507@yandex.ru");
    private Account account;

    Accounts(String login, String password, String email) {
        account = new Account(login, password, email);
    }

    public Account getAccount() {
        return account;
    }
}
