package pages;

import org.openqa.selenium.By;
import reporting.Logger;

/**
 * Created by Art on 13.07.2015.
 */
public class MailboxBasePage extends AbstractBasePage { 
    private static final By USER_LOGIN_NAME_LOCATOR = By.xpath("//span[@class='header-user-name js-header-user-name']");

    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    private static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    private static final By DELETED_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    private static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");

    public MailInboxPage openInboxPage(){
        Logger.log.info("Open inbox page");
        browser.click(INBOX_LINK_LOCATOR);
        return new MailInboxPage();
    }

    public MailSentPage openSentPage(){
        Logger.log.info("Open sent page");
        browser.click(SENT_LINK_LOCATOR);
        browser.waitForVisible(SENT_LINK_LOCATOR);
        return new MailSentPage();
    }

    public MailDeletedPage openDeletedPage(){
        Logger.log.info("Open delete page");
        browser.click(DELETED_LINK_LOCATOR);
        return new MailDeletedPage();
    }

    public MailSpamPage openSpamPage(){
        Logger.log.info("Open spam page");
        browser.click(SPAM_LINK_LOCATOR);
        return new MailSpamPage();
    }

    public String getUserLoginName(){
        Logger.log.info("Get user name");
        return browser.getText(USER_LOGIN_NAME_LOCATOR);
    }
}