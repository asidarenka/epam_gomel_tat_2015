package pages;

import org.openqa.selenium.By;
import ui.Browser;

/**
 * Created by Art on 14.09.2015.
 */
public class YandexPassportPage {
    public static final By MESSAGE_LOCATOR = By.xpath("//div[@class='error-msg']");
    private static final String ERROR_TEXT = "������������ ����� ��� ������.";

    public boolean isFindMessage(){
        return Browser.get().getText(MESSAGE_LOCATOR).equals(ERROR_TEXT);
    }
}
