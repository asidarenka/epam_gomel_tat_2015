package pages;

import org.openqa.selenium.By;
import ui.Browser;

/**
 * Created by Art on 23.08.2015.
 */
public class YandexDiskTrashPage {
    private static final String FILE_LOCATOR_PATTERN = "//div[contains(@class,'ns-view-listing')][contains(@data-key,'trash')]//div[contains(@class,'nb-resource')][contains(@title,'%s')]";
    private static final String RESTORE_BUTTON_LOCATOR = "//div[contains(@class,'nb-panel__title')]//ancestor::div[contains(@class,'ns-view-aside')][contains(@data-key,'%s')]//button[contains(@class,'nb-button')][@data-click-action='resource.restore']";
    private static final By BASE_PAGE_LOCATOR = By.xpath("//a[contains(@class,'b-crumbs__root')]");

    public YandexDiskTrashPage restoreFile(String name){
        Browser browser = Browser.get();
        browser.click(By.xpath(String.format(FILE_LOCATOR_PATTERN, name)));
        browser.click(By.xpath(String.format(RESTORE_BUTTON_LOCATOR, name)));
        Browser.get().waitForAjaxProccessed();
        return new YandexDiskTrashPage();
    }

    public YandexDiskBasePage openBasePage(){
        Browser.get().click(BASE_PAGE_LOCATOR);
        return new YandexDiskBasePage();
    }
}
