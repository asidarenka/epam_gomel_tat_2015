package pages;

import bo.mail.MailLetter;
import cli.TestRunnerOption;
import org.openqa.selenium.By;
import reporting.Logger;
import utils.GenerationLetter;

import java.io.IOException;

/**
 * Created by Art on 13.07.2015.
 */
public class ComposeMailPage extends AbstractBasePage { 
    private static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    private static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    private static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    private static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    private static final By ATTACHE_FILE_INPUT_LOCATOR = By.xpath("//input[@name='att']");

    public MailboxBasePage sendMail(String mailTo, String subject, String content){
        Logger.log.info("Send mail");
        browser.type(TO_INPUT_LOCATOR, mailTo);
        browser.type(SUBJECT_INPUT_LOCATOR, subject);
        browser.type(MAIL_TEXT_LOCATOR, content);
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
        browser.waitForAjaxProccessed();
        return new MailboxBasePage();
    }

    public MailboxBasePage sendMailWithAttach(String mailTo, String subject, String content, String fileName){
        Logger.log.info("Send mail with attach");
        browser.type(TO_INPUT_LOCATOR, mailTo);
        browser.type(SUBJECT_INPUT_LOCATOR, subject);
        browser.type(MAIL_TEXT_LOCATOR, content);
        GenerationLetter.randomFile(TestRunnerOption.DOWNLOAD_DIR + fileName, 20);
        browser.attachFile(ATTACHE_FILE_INPUT_LOCATOR, TestRunnerOption.DOWNLOAD_DIR + fileName);
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
        browser.waitForAjaxProccessed();
        return new MailboxBasePage();
    }
}