package pages;

import org.openqa.selenium.By;

/**
 * Created by Art on 10.08.2015.
 */
public class LoginDiskPage extends AbstractBasePage {
    public static final String START_PAGE = "http://www.disk.yandex.ru";
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("password");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//*[@type='submit']");

    public LoginDiskPage open(){
        browser.open(START_PAGE);
        return this;
    }

    public YandexDiskBasePage login(String login, String password){
        browser.type(LOGIN_INPUT_LOCATOR, login);
        browser.type(PASSWORD_INPUT_LOCATOR, password);
        browser.click(LOGIN_BUTTON_LOCATOR);
        return new YandexDiskBasePage();
    }
}
