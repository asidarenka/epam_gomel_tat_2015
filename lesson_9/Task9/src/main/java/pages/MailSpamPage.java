package pages;

import org.openqa.selenium.By;

/**
 * Created by Art on 13.07.2015.
 */
public class MailSpamPage extends AbstractBasePage { 
    private static final String MESSAGE_LINK_LOCATOR_PATTERN = "//label[text()='Spam']/ancestor::div[@class='block-messages']//a[.//*[text()='%s']]";
    private static final By NOT_SPAM_LINK_LOCATOR = By.xpath("//a[@title='Not spam!']");

    public MailSpamPage openMessage(String subject){
        String mailLocator = String.format(MESSAGE_LINK_LOCATOR_PATTERN, subject);
        browser.click(By.xpath(mailLocator));
        return new MailSpamPage();
    }

    public void markAsNotSpam(){
        browser.waitForAjaxProccessed();
        browser.click(NOT_SPAM_LINK_LOCATOR);
        browser.waitForAjaxProccessed();
    }
}