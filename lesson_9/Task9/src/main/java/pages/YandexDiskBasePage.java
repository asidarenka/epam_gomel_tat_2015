package pages;

import org.omg.CORBA.TIMEOUT;
import org.openqa.selenium.By;
import ui.Browser;

/**
 * Created by Art on 10.08.2015.
 */
public class YandexDiskBasePage {
    private static final By UPLOAD_FILE_BUTTON_LOCATOR = By.xpath("//input[@class='_nb-file-intruder-input']");
    private static final By CLOSE_COMPLETED_DIALOG_BUTTON_LOCATOR = By.xpath("//a[@class='_nb-popup-close ns-action js-cross']");
    private static final String FILE_LOCATOR_PATTERN = "//div[contains(@class,'ns-view-listing')][contains(@data-key,'disk')]//div[contains(@class,'nb-resource')][contains(@title,'%s')]";
    private static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN = "//div[contains(@class,'nb-panel__title__name')][contains(text(),'%s')]//ancestor::div[contains(@class,'nb-panel__content')]//button[@data-click-action='resource.download']";
    private static final By TRASH_BOX_LOCATOR = By.xpath("//div[contains(@class,'ns-view-listing')][contains(@data-key,'disk')]//div[@data-id='/trash']/div");

    private static final String FILE_LINK_PATTERN = "//div[1][@title='%s']";

    public YandexDiskBasePage uploadFile(String pathToFile, String n) {
        Browser browser = Browser.get();
        browser.attachFile(UPLOAD_FILE_BUTTON_LOCATOR, pathToFile);
        browser.waitForVisible(CLOSE_COMPLETED_DIALOG_BUTTON_LOCATOR);
        browser.click(CLOSE_COMPLETED_DIALOG_BUTTON_LOCATOR);
        browser.waitForAjaxProccessed();
        browser.waitForVisible(By.xpath(String.format(FILE_LINK_PATTERN, n)));
        return new YandexDiskBasePage();
    }

    public YandexDiskBasePage downloadFile(String fileName){
        try {
            Thread.sleep(2 * 1000);
        } catch (InterruptedException e) {
        }
        Browser browser = Browser.get();
        Browser.get().waitForAjaxProccessed();
        browser.click(By.xpath(String.format(FILE_LOCATOR_PATTERN, fileName)));
        Browser.get().waitForAjaxProccessed();
        browser.click(By.xpath(String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN, fileName)));
        return new YandexDiskBasePage();
    }

    public YandexDiskBasePage goToTrashFile(String name){
        Browser browser = Browser.get();
        browser.dragAndDrop(By.xpath(String.format(FILE_LOCATOR_PATTERN, name)), TRASH_BOX_LOCATOR);
        browser.waitForAjaxProccessed();
        return new YandexDiskBasePage();
    }

    public YandexDiskTrashPage openTrashPage(){
        Browser browser = Browser.get();
        browser.doubleClick(TRASH_BOX_LOCATOR);
        browser.waitForAjaxProccessed();
        return new YandexDiskTrashPage();
    }

    public boolean isFilePresent(String name){
        return Browser.get().isPresent(By.xpath(String.format(FILE_LOCATOR_PATTERN, name)));
    }
}
