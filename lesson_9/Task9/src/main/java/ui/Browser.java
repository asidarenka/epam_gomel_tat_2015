package ui;

import cli.TestRunnerOption;
import com.google.common.base.Predicate;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import reporting.Logger;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Art on 13.07.2015.
 */
public class Browser {
    public static final int WAIT_ELEMENT_TIME_OUT = 20;
    private static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt";
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 5;
    private static final int AJAX_TIMEOUT = 20;
    public static final String URL_PATTERN = "http://%s:%d/wd/hub";
    public static final String SCREENSHOT_DIRECTORY = "./screenshots";
    private static final String SCREENSHOT_LINK_PATTERN = "<a href=\"file:///%s\">Screenshot</a>";
    private WebDriver driver;
    private boolean augmented;
    private static Map<Thread, Browser> instances = new HashMap<Thread, Browser>();
    private static Browser instance = null;

    private Browser(final WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            return instance;
        }
        instance = init();
        instances.put(currentThread, instance);
        return instance;
    }

    private static Browser init() {
        BrowserType browserType = TestRunnerOption.getBrowserType();
        WebDriver driver = null;
        switch(browserType){
            case REMOTE:
                Logger.info("Open REMOTE browser");
                try{
                    driver = new RemoteWebDriver(new URL(String.format(URL_PATTERN, TestRunnerOption.getInstance().getHost(),
                            TestRunnerOption.getInstance().getPort())), DesiredCapabilities.firefox());
                }catch(MalformedURLException e){
                    throw new RuntimeException("Invalid URL format");
                }
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case FIREFOX:
                Logger.info("Open FIREFOX browser");
                driver = new FirefoxDriver(getFireFoxProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case CHROME:
                Logger.info("Open CHROME browser");
                System.setProperty("webdriver.chrome.driver", TestRunnerOption.getChromeDriver());
                driver = new ChromeDriver(getChromeProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS*2, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS*2, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
        }
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", TestRunnerOption.getDownloadDir());
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    private static DesiredCapabilities getChromeProfile(){
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", TestRunnerOption.DOWNLOAD_DIR);
        chromePrefs.put("download.prompt_for_download", false);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        return capabilities;
    }

    public void open(String url) {
        Logger.info("Open URL");
        driver.get(url);
        Browser.get().takeScreenshot();
    }

    public static void kill() {
        Logger.info("Brower close");
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            try {
                instance.driver.quit();
            } catch (Exception e) {
                Logger.error("Can not kill browser", e);
            } finally {
                instances.remove(currentThread);
            }
        }
    }

    public void takeScreenshot() {
        if (TestRunnerOption.getInstance().getBrowserType() == BrowserType.REMOTE && !augmented) {
            driver = new Augmenter().augment(driver);
            augmented = true;
        }
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            File copy = new File(SCREENSHOT_DIRECTORY + "/" + System.nanoTime() + ".png");
            FileUtils.copyFile(screenshot, copy);
            Logger.info(String.format(SCREENSHOT_LINK_PATTERN, copy.getAbsolutePath().replace("\\", "/")));
        } catch (IOException e) {
            Logger.error("Failed to copy screenshot", e);
        }
    }

    public String getText(final By locator) {
        Logger.info("Get text from : " + locator);
        waitForPresent(locator);
        takeScreenshot();
        return driver.findElement(locator).getText();
    }

    public void click(final By locator) {
        Logger.info("Click : " + locator);
        waitForPresent(locator);
        driver.findElement(locator).click();
        takeScreenshot();
    }

    public void doubleClick(By locator){
        Logger.info("Double click : " + locator.toString());
        WebElement element = driver.findElement(locator);
        new Actions(driver).doubleClick(element).build().perform();
        takeScreenshot();
    }

    public void type(final By locator,final String text) {
        Logger.info("Find element : " + locator + "SendKeys :" + text);
        waitForPresent(locator);
        driver.findElement(locator).sendKeys(text);
        takeScreenshot();
    }

    public void attachFile(final By locator, String text) {
        Logger.info("Find element for attach : " + locator + "SendKeys :" + text);
        waitForPresent(locator);
        driver.findElement(locator).sendKeys(text);
        takeScreenshot();
    }

    public void dragAndDrop(By fromLocator,By toLocator){
        Logger.info("Element : " + fromLocator.toString() + "dragAndDrop to element : " + toLocator.toString());
        waitForPresent(fromLocator);
        WebElement fromElement = driver.findElement(fromLocator);
        takeScreenshot();
        waitForPresent(toLocator);
        WebElement toElement = driver.findElement(toLocator);
        takeScreenshot();
        new Actions(driver).dragAndDrop(fromElement, toElement).build().perform();
        takeScreenshot();
    }

    public boolean isPresent(final By locator){
        return driver.findElements(locator).size() > 0;
    }

    public void waitForPresent(final By locator){
        new WebDriverWait(driver, WAIT_ELEMENT_TIME_OUT).until(new Predicate<WebDriver>() {
            public boolean apply(WebDriver webDriver) {
                return isPresent(locator);
            }
        });
    }

    public void waitForVisible(By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIME_OUT).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    public void waitForAjaxProccessed() {
        Logger.info("Wait for ajax proccessed");
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void waitForDownload(File file){
        new FluentWait<File>(file).withTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS).until(new Predicate<File>() {
            public boolean apply(File file) {
                return file.exists();
            }
        });
    }

    public void refresh() {
        Logger.info("Refresh page");
        driver.navigate().refresh();
        takeScreenshot();
    }
}