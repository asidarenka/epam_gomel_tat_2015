package com.epam.tat.lesson9.test.atu;


import atu.testng.reports.ATUReports;
import atu.testng.reports.listeners.ATUReportsListener;
import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import com.epam.tat.lesson9.browser.Browser;
import com.epam.tat.lesson9.utils.FileTools;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

@Listeners({ ATUReportsListener.class, ConfigurationListener.class,
        MethodListener.class })
public class TestNGScript {


    //Set Property for ATU Reporter Configuration
    {
        System.setProperty("atu.reporter.config", FileTools.getCanonicalPathToResourceFile("/atu.properties"));
    }

    WebDriver driver;

    @BeforeClass
    public void init() {
        // Inside browser will be installed driver into ATUReports
        Browser.get(); // See line Browser:106
        setIndexPageDescription();
    }

    private void setAuthorInfoForReports() {
        ATUReports.setAuthorInfo("Automation Tester", Utils.getCurrentTime(),
                "1.0");
    }

    private void setIndexPageDescription() {
        ATUReports.indexPageDescription = "My Project Description <br/> <b>Can include Full set of HTML Tags</b>";
    }

    //Deprecated Methods
    @Test
    public void testME() {
        setAuthorInfoForReports();
        ATUReports.add("Step Desc", false);
        ATUReports.add("Step Desc", "inputValue", false);
        ATUReports.add("Step Desc", "expectedValue", "actualValue", false);
        ATUReports.add("Step Desc", "inputValue", "expectedValue",
                "actualValue", false);
    }

    //New Way of Logging
    @Test
    public void testNewLogs() throws AWTException, IOException {
        AtuLogger.info("ok");
        Browser.get().open("http://www.google.ru");
        AtuLogger.info("take page screenshot", true);
        Browser.get().takeScreenshot();
        ATUReports.add("INfo Step", LogAs.INFO, new CaptureScreen(
                CaptureScreen.ScreenshotOf.BROWSER_PAGE));
        ATUReports.add("Pass Step", LogAs.PASSED, new CaptureScreen(
                CaptureScreen.ScreenshotOf.DESKTOP));
//      WebElement element = driver
//              .findElement(By.xpath("/html/body/div/h1/a"));
//      ATUReports.add("Warning Step", LogAs.WARNING, null);
//      ATUReports.add("Fail step", LogAs.FAILED, new CaptureScreen(
//              CaptureScreen.ScreenshotOf.DESKTOP));
    }

}