package case4;

import Utils.YandexUtils;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class SendMailWithAttachment {
    private WebDriver driver;

    public static final String START_PAGE = "http://www.ya.ru"; //Start page

    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_LOCATOR = By.name("login");
    public static final By PASSWORD_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//input[@id='nb-23']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By SEND_INPUT_LOCATOR = By.name("send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By ATT_BUTTON_LOCATOR = By.xpath("//input[@name='att']");
    public static final By LAST_MESSAGE_LOCATOR = By.xpath("//span[@class='b-messages__subject']");
    public static final By INPOX_LINK_LOCATOR = By.xpath("//a[contains(@class,'b-done-redirect__link')]");
    public static final By DOWNLOAD_LINK_LOCATOR = By.xpath("//a[@class='b-link b-link_w b-link_js b-file__download js-attachments-get-btn daria-action']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[.//*[text()='%s']]";


    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final int PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int WAIT_TIMEOUT_SECONDS = 5;

    private String userLogin = "test201507";
    private String userPassword = "test2015";
    private String originMail = "test201507@yandex.ru";
    private String theme = YandexUtils.randomTheme();
    private String textOfMessage = YandexUtils.randomMessage();
    private String nameFile = "File" + (int)(Math.random()*100)+".txt";
    private String pathToFile = "D:\\TMP\\"+ nameFile;
    private String downloadedPath = "D:\\" + nameFile;

    @BeforeTest(description = "prepare Browser")
    public void setUp(){
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", "D:\\");
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/xml,text/plain,text/xml,image/jpeg");
        YandexUtils.randomFile(pathToFile);


        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @BeforeTest(description = "Login to mail", dependsOnMethods = "setUp")
    public void login() {
        driver.navigate().to(START_PAGE);
        driver.findElement(ENTER_BUTTON_LOCATOR).click();
        driver.findElement(LOGIN_LOCATOR).sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
    }

    @BeforeTest(description = "Send message", dependsOnMethods = {"login"})
    public void sendMail() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        driver.findElement(TO_INPUT_LOCATOR).sendKeys(originMail);
        driver.findElement(SUBJECT_INPUT_LOCATOR).sendKeys(theme);
        driver.findElement(SEND_INPUT_LOCATOR).sendKeys(textOfMessage);
        new WebDriverWait(driver, 5000).until(ExpectedConditions.invisibilityOfElementLocated(ATT_BUTTON_LOCATOR));
        driver.findElement(ATT_BUTTON_LOCATOR).sendKeys(pathToFile);
        driver.findElement(SEND_MAIL_BUTTON_LOCATOR).click();
        WebElement inboxLink = driver.findElement(INPOX_LINK_LOCATOR);
        inboxLink.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, theme))));
    }

    @Test(description = "Download attachment file")
    public void downloadFile(){
        driver.findElements(LAST_MESSAGE_LOCATOR).get(0).click();
        driver.findElement(DOWNLOAD_LINK_LOCATOR).click();
    }

    @Test(description = "Check files", dependsOnMethods = {"downloadFile"})
    public void checkContent() throws IOException {
        File originFile = new File(pathToFile);
        File downloadFile = new File(downloadedPath);
        driver.navigate().refresh();
        Assert.assertTrue(FileUtils.contentEquals(originFile, downloadFile), "File changed!");
        downloadFile.delete();
        originFile.delete();

    }

    @AfterClass(description = "Close browser")
    public void tearDown(){
        driver.quit();
    }

    private WebElement waitForElement(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}
