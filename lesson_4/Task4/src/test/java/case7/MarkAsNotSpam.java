package case7;

import Utils.YandexUtils;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class MarkAsNotSpam {

    WebDriver driver;

    public static final String START_PAGE = "http://www.ya.ru";

    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_LOCATOR = By.name("login");
    public static final By PASSWORD_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//input[@id='nb-23']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By SEND_INPUT_LOCATOR = By.name("send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INPOX_LINK_LOCATOR = By.xpath("//a[contains(@class,'b-done-redirect__link')]");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[.//*[text()='%s']]";
    public static final String CHECKBOX__LOCATOR_PATTERN = "//span[@class = 'b-messages__subject'][text()='%s']/ancestor::div[1]//input[@type = 'checkbox']";
    public static final By LAST_MESSAGE_LOCATOR = By.xpath("//span[@class='b-messages__subject']");
    public static final By SPAM_BUTTON_LOCATOR = By.xpath("//div[@class='b-toolbar__i']//a[@data-action='tospam']");
    public static final By SPAM_LIST_LOCATOR = By.xpath("//a[@href = '#spam']");
    public static final By NOT_SPAM_BUTTON_LOCATOR = By.xpath("//a[@class = 'b-toolbar__item b-toolbar__item_not-spam js-toolbar-item-not-spam daria-action']");
    public static final By INBOX_LOCATOR = By.xpath("//a[@href = '#inbox']");

    private String userLogin = "test201507";
    private String userPassword = "test2015";
    private String originMail = "test201507@yandex.ru";
    private String theme = YandexUtils.randomTheme();
    private String textOfMessage = YandexUtils.randomMessage();

    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final int PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int WAIT_TIMEOUT_SECONDS = 10;

    @BeforeTest(description = "Prepare browser")
    public void setUp(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @BeforeTest(description = "Login to mail", dependsOnMethods = "setUp")
    public void loginToMail() {
        driver.navigate().to(START_PAGE);
        driver.findElement(ENTER_BUTTON_LOCATOR).click();
        driver.findElement(LOGIN_LOCATOR).sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
    }

    @BeforeTest(description = "Login to mail", dependsOnMethods = "loginToMail")
    public void sendMail() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        driver.findElement(TO_INPUT_LOCATOR).sendKeys(originMail);
        driver.findElement(SUBJECT_INPUT_LOCATOR).sendKeys(theme);
        driver.findElement(SEND_INPUT_LOCATOR).sendKeys(textOfMessage);
        driver.findElement(SEND_MAIL_BUTTON_LOCATOR).click();
        WebElement inboxLink = driver.findElement(INPOX_LINK_LOCATOR);
        inboxLink.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, theme))));
    }

    @Test(description = "Mark email as a spam")
    public void markAsSpam(){
        driver.findElements(LAST_MESSAGE_LOCATOR).get(0).click();
        driver.findElement(SPAM_BUTTON_LOCATOR).click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(SPAM_BUTTON_LOCATOR));
        waitForAjaxProcessed();
    }

    @Test(description = "check", dependsOnMethods = "markAsSpam")
    public void checkSpam(){
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(SPAM_LIST_LOCATOR));
        WebElement spamElement1 = driver.findElement(SPAM_LIST_LOCATOR);
        spamElement1.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, theme))));
    }

    @Test(description = "Mark as not spam", dependsOnMethods = "checkSpam")
    public void markNotSpam(){
        WebElement checkBoxInput = driver.findElement(By.xpath(String.format(CHECKBOX__LOCATOR_PATTERN, theme)));
        checkBoxInput.click();
        WebElement notSpamButton = driver.findElement(NOT_SPAM_BUTTON_LOCATOR);
        notSpamButton.click();
        waitForAjaxProcessed();
    }

    @Test(description = "Check message inbox", dependsOnMethods = "markNotSpam")
    public void checkMessage(){
        WebElement sentButton = driver.findElement(INBOX_LOCATOR);
        sentButton.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, theme))));
    }

    @AfterTest(description = "Close browser")
    public void tearDown(){
        driver.quit();
    }

    public void waitForAjaxProcessed() {
        new WebDriverWait(driver, 20).until(new Predicate<WebDriver>() {
            //@Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }
}
