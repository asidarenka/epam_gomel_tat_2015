package case2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class UnsuccessLoginToYandex {

    private WebDriver driver;

    public static final String START_PAGE = "http://www.ya.ru"; //Start page

    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_LOCATOR = By.name("login");
    public static final By PASSWORD_LOCATOR = By.name("passwd");
    public static final By MESSAGE_LOCATOR = By.xpath("//div[@class='error-msg']");

    public static final int PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int WAIT_TIMEOUT_SECONDS = 5;

    private String userLogin = "test201507";
    private String userPassword = "qwer";
    private String errorText = "������������ ���� �����-������! �������������� �� �������.";


    @BeforeTest(description = "prepare Browser")
    public void setUp(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Test(description = "login to mail")
    public void loginToMail(){
        driver.navigate().to(START_PAGE);
        driver.findElement(ENTER_BUTTON_LOCATOR).click();
        driver.findElement(LOGIN_LOCATOR).sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        WebElement errorElement =  driver.findElement(MESSAGE_LOCATOR);
        Assert.assertEquals(errorElement.getText(), errorText);

    }

    @AfterClass(description = "Close browser")
    public void tearDown(){
        driver.quit();
    }
}
