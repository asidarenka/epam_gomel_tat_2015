package case5;

import Utils.YandexUtils;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Art on 07.07.2015.
 */
public class DeleteEmail {

    WebDriver driver;

    public static final String START_PAGE = "http://www.ya.ru";

    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_LOCATOR = By.name("login");
    public static final By PASSWORD_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//input[@id='nb-23']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By SEND_INPUT_LOCATOR = By.name("send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INPOX_LINK_LOCATOR = By.xpath("//a[contains(@class,'b-done-redirect__link')]");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[.//*[text()='%s']]";
    public static final By LAST_MESSAGE_LOCATOR = By.xpath("//span[@class='b-messages__subject']");
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//span[@class='b-toolbar__item__label js-toolbar-item-title-delete']");
    public static final By DELETED_LIST_LOCATOR = By.xpath("//a[@href='#trash']");

    private String userLogin = "test201507";
    private String userPassword = "test2015";
    private String originMail = "test201507@yandex.ru";
    private String theme = YandexUtils.randomTheme();
    private String textOfMessage = YandexUtils.randomMessage();

    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final int PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int WAIT_TIMEOUT_SECONDS = 5;

    @BeforeTest(description = "Prepare browser")
    public void setUp(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @BeforeTest(description = "Login to mail", dependsOnMethods = "setUp")
    public void loginToMail() {
        driver.navigate().to(START_PAGE);
        driver.findElement(ENTER_BUTTON_LOCATOR).click();
        driver.findElement(LOGIN_LOCATOR).sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
    }

    @BeforeTest(description = "Login to mail", dependsOnMethods = "loginToMail")
    public void sendMail() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        driver.findElement(TO_INPUT_LOCATOR).sendKeys(originMail);
        driver.findElement(SUBJECT_INPUT_LOCATOR).sendKeys(theme);
        driver.findElement(SEND_INPUT_LOCATOR).sendKeys(textOfMessage);
        driver.findElement(SEND_MAIL_BUTTON_LOCATOR).click();
        WebElement inboxLink = driver.findElement(INPOX_LINK_LOCATOR);
        inboxLink.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, theme))));
    }

    @Test(description = "Delete message")
    public void deleteMessage(){
        driver.findElements(LAST_MESSAGE_LOCATOR).get(0).click();
        driver.findElement(DELETE_BUTTON_LOCATOR).click();
        waitForAjaxProcessed();

    }

    @Test(description = "Check message in deleted list", dependsOnMethods = "deleteMessage" )
    public void checkMessage(){
        driver.findElement(DELETED_LIST_LOCATOR).click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, theme))));
    }

    @AfterTest(description = "Close browser")
    public void tearDown(){
        driver.quit();
    }

    public void waitForAjaxProcessed() {
        new WebDriverWait(driver, 20).until(new Predicate<WebDriver>() {
            //@Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }
}
