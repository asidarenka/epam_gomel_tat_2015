package Utils;

import org.apache.commons.lang3.RandomStringUtils;
import java.io.*;


public class YandexUtils {

    public static final int THEME_LENGTH = 5;
    public static final int MESSAGE_LENGTH = 20;
    public static final int TEXT_LENGTH = 20;

    public static String randomTheme(){
        return RandomStringUtils.randomAlphanumeric(THEME_LENGTH);
    }

    public static String randomMessage(){
        return RandomStringUtils.randomAlphanumeric(MESSAGE_LENGTH);
    }


    public static String randomFile(String path){
        FileOutputStream output;
        try {
            output = new FileOutputStream(path);
            output.write(RandomStringUtils.randomAlphanumeric(MESSAGE_LENGTH).getBytes());
            output.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return RandomStringUtils.randomAlphanumeric(MESSAGE_LENGTH);
    }
}
